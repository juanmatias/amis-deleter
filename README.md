# AMI Deleter

This script will query for an AMI name (adding a * at the end), and for images created before a given date.

Then it allows to delete them.

The steps for each image is:

  - unregister the image
  - delete related snapshots

## How to run

Example for dry run:

```
./delete-old-amis.sh amibasename 2019-12-31 us-east-1
```

Example for run it deleting images:

```
./delete-old-amis.sh amibasename 2019-12-31 us-east-1  --apply
```

More help with:

```
./delete-old-amis.sh -h
```

## Argbash

This script was made using [Argbash](argbash.io), so if you need to modify it, please modify the `m4` file and then recreate the script with:

```
argbash delete-old-amis.m4 -o delete-old-amis.sh
```
