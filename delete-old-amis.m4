#!/bin/bash

# m4_ignore(
echo "This is just a script template, not the script (yet) - pass it to 'argbash' to fix this." >&2
exit 11  #)Created by argbash-init v2.8.1
# ARG_OPTIONAL_BOOLEAN([apply],[],[Whether or not apply actually the deletions.])
# ARG_OPTIONAL_BOOLEAN([debug],[],[Print more info.])
# ARG_POSITIONAL_SINGLE([ami-name-base],[Base name for AMI, e.g. myname to look for AMI-Name=myname*])
# ARG_POSITIONAL_SINGLE([before-day],[Look for images with creation date until before-day, e.g. 2020-01-31])
# ARG_POSITIONAL_SINGLE([aws-region],[AWS Region, e.g. us-east-1])
# ARG_DEFAULTS_POS
# ARG_HELP([<The general help message of my script>])
# ARGBASH_GO

# [ <-- needed because of Argbash

if [ "$_arg_debug" = "on" ];
then
	printf "'%s' is %s\\n" 'apply' "$_arg_apply"
	printf "Value of '%s': %s\\n" 'ami-name-base' "$_arg_ami_name_base"
	printf "Value of '%s': %s\\n" 'before-day' "$_arg_before_day"
	printf "Value of '%s': %s\\n\\n" 'aws-region' "$_arg_aws_region"
fi

echo "* ******************************** *"
echo "* Delete old AMIs from AWS account *"
echo "* ******************************** *"
echo " "
if [ "$_arg_apply" = "off" ];
then
	echo "Running without apply the actual commands!"
	echo " "
fi

cmd="aws ec2 describe-images --region $_arg_aws_region --owners self --filters \"Name=name,Values="$_arg_ami_name_base"*\"  --query 'Images[?CreationDate<\`$_arg_before_day\`] | sort_by(@, &CreationDate)[].[ImageId,BlockDeviceMappings[*].Ebs.SnapshotId,CreationDate]'"
images=$(eval $cmd)
if [ $(echo $images|jq '. | length') -eq 0 ];
then
	echo "No images, nothing to do."
	echo " "
	exit 0
fi
if [ "$_arg_debug" = "on" ];
then
	echo "Getting images:"
	echo $cmd
	echo " "
fi

for k in $(jq '. | keys | .[]' <<< "$images");
do
        image=$(jq -r ".[$k][0]"  <<< "$images")
	snapshots=$(jq -r ".[$k][1]"  <<< "$images")
	creationdate=$(jq -r ".[$k][2]"  <<< "$images")
	if [ "$_arg_debug" = "on" ];
	then
		echo "Index: $k"
		echo "Processing $image"
		echo "  Snapshots $snapshots"
		echo "  Date $creationdate"
		echo " "
	fi
	echo "Unregistering:"
	echo aws ec2 deregister-image --image-id $image --region $_arg_aws_region 
	if [ "$_arg_apply" = "on" ];
	then
		aws ec2 deregister-image --image-id $image --region $_arg_aws_region 
	fi
	echo "Deleting Snapshots:"
        for s in $(jq '. | keys | .[]' <<< $snapshots);
        do
		echo aws ec2 delete-snapshot --snapshot-id $(jq -r ".[$s]" <<< $snapshots) --region $_arg_aws_region 
		if [ "$_arg_apply" = "on" ];
		then
			aws ec2 delete-snapshot --snapshot-id $(jq -r ".[$s]" <<< $snapshots) --region $_arg_aws_region 
		fi
		echo " "
	done
done
# ] <-- needed because of Argbash
